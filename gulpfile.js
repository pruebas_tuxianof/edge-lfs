/**
 * @author Luis Felipe Zapata <lacri@misena.edu.co>
 * @version 0.0.3
 * @description
 * CÃ³digo para generar la carpeta public de la multimedia que permite su
 * ejecuciÃ³n en un equipo de forma local con solo abrir el archivo HTML.
 * Se realiza una copia de archivos JS, HTML y JSON. Se minifican y mueven a la
 * ruta de la carpeta public, genera un archivo ZIP con la versiÃ³n descargable
 * de la multimedia.
 *
 * @tutorial https://paper.dropbox.com/doc/Configuracion-de-publicacion-de-multimedias-HwtustPIvE7exlyxaNb4g
 */


//#region DeclaraciÃ³n de constantes.

const gulp = require('gulp');
const gulpEach = require('gulp-each');
const gulpJsonMinify = require('gulp-json-minify');
const gulpHtmlMinify = require('gulp-htmlmin');
const gulpFile = require('gulp-file');
const gulpCopy = require('gulp-copy');
const gulpZip = require('gulp-zip');
const gulpConcat = require('gulp-concat');
const gulpRename = require('gulp-rename');
const gulpUglify = require('gulp-uglifyjs');
const gulpClean = require('gulp-clean');
const gulpMenu = require('gulp-task-menu');
const gulpSequence = require('gulp-sequence');
const gulpReplace = require('gulp-replace');
const gulpWait = require('gulp-wait');
const clear = require('clear');

//#endregion

//#region DeclaraciÃ³n de variables iniciales.

/**
 * @type {String} [folderPublic=public] - Carpeta donde se almacenarÃ¡ el cÃ³digo 
 * de la multimedia lista para producciÃ³n incluyendo el archivo ZIP con la
 * versiÃ³n descargable de la multimedia.
 */
let folderPublic = 'public';

/**
 * @type {String} [folderDevelop=develop] - Carpeta donde se almacenarÃ¡ el cÃ³digo 
 * de la multimedia que se utiliza para producciÃ³n.
 */
let folderDevelop = 'develop';

/**
 * @type {String} [folderJSON=json] - Carpeta donde se almacena los archivos
 * JSON de configuraciÃ³n de plantillas.
 */
let folderJSON = 'json';

/**
 * @type {String} [folderJ=js] - Carpeta donde se almacenara el codigo JS 
 * generado para integrar con los archivos de configuraciÃ³n y HTML.
 */
let folderJS = 'js';

/**
 * @type {String} [folderHTML=texts= - Carpeta donde se almacena los archivos 
 * HTML que realizan la carga del texto.
 */
let folderHTML = 'texts';

/**
 * @type {String} [nameFileConfig=config] - Nombre del archivo JS que almacena las
 * configuraciones y textos HTML que se usan en la multimedia.
 */
let nameFileConfig = 'config';

/**
 * @type {String} [nameFileMain=main] - Nombre del archivo JS que almacena
 * todos los JS minificados y ofuscados.
 */
let nameFileMain = 'main';

/**
 * @type {String} [nameFileDevelop=develop-NO-OPEN] - Nombre del archivo JS
 * que almacena todos los JS que son utilizados para la versiÃ³n de desarrollo.
 */
let nameFileDevelop = 'develop-NO-OPEN';

/**
 * @type {String} [nameFileZIP=OVA.ZIP] - Nombre del archivo ZIP que contiene
 * la versiÃ³n descargable de la multimedia.
 */
let nameFileZIP = 'OVA.zip';

/**
 * @type {Object<Object>} listFileJSON - Almacena la ruta absoluta del archivo
 * JSON y como valor asigna su contenido.
 */
let listFileJSON = {};

/**
 * @type {Object<String>} listFileHTML - Almacena la ruta absoluta del archivo
 * HTML y asigna como srting el contenido del documento.
 */
let listFileHTML = {};

/**
 * @type {Array<>} filesCombine - Listado de archivos para combinar.
 */
let filesCombine = [
  `./edge_includes/jquery-2.0.3.min.js`,
  `./edge_includes/edge.4.0.1.min.js`,
  `./${folderJS}/soundList.js`,
  `./${folderDevelop}/${nameFileConfig}.js`,
  `./${folderJS}/libs/TweenMax.min.js`,
  `./${folderJS}/libs/CodeCraft.min.js`,
  `./${folderJS}/libs/**/*.js`,
  `./${folderJS}/src/**/Templates.min.js`,
  `./${folderJS}/src/**/*.js`,
  `./${folderJS}/*.js`,
  `index_edge.js`,
  `index_edgeActions.js`
];

/**
 * @description
 * Tarea por defecto de GULP, ejecuta un llamado al menÃº de opciones para 
 * realizar la carga del entorno de desarrollo o producciÃ³n de la multimedia.
 */
gulp.task('default', done => {
  clear();
  gulpMenu(gulp, [
    'default',
    'removeOldFilesProduction',
    'removeOldFilesDevelop',
    'buildDevelop',
    'createConfig',
    'createZip',
    'modifyPreloadEdge',
    'modifyPreloadEdgeDevelop',
    'copyPublic',
    'combineMinifyJsDevelop',
    'combineMinifyJsProduction',
    'combineMinifyJsProductionDownload',
    'createConfigProduction',
    'readHtml',
    'readJson',
    'watchChange'
  ]);
});

//#endregion

//#region Funciones globales o de uso generico.

/**
 * @description
 * Crea el archivo de configuraciÃ³n en la carpeta donde se almacena el cÃ³dgo JS.
 */
gulp.task('createConfig', ['readHtml'], done => {
  return gulp
    .src(`./${folderJS}/*.js`, {
      read: false
    })
    .pipe(gulpFile(`${nameFileConfig}.js`, createContentFileConfig()))
    .pipe(gulp.dest(`./${folderDevelop}`));
});

/**
 * @description
 * Similar a la funcion createContentFileConfigDownload pero asigna el valor
 * false en a variable de versiÃ³n descargable para activar la opciÃ³n de ZIP
 * descargable.
 * @return {String} Cadena de caracteres con variables utilizadas para
 * indentificar el contenido de los archivos.
 */
function createContentFileConfig() {
  var contentFile = `
  var versionDownload = false;
  var jsonFiles = ${JSON.stringify(listFileJSON)};
  var htmlFiles = ${JSON.stringify(listFileHTML)};
  `;
  return contentFile;
}

/**
 * @description
 * Realiza la creaciÃ³n del listado de los archivos HTML que son utilizados para
 * la inserciÃ³n del texto en la multimedia.
 */
gulp.task('readHtml', ['readJson'], done => {
  return gulp
    .src(`./${folderHTML}/**/*.html`)
    // Minifica el contenido del archivo HTMl
    .pipe(gulpHtmlMinify({
      collapseWhitespace: true
    }))
    .pipe(gulpEach((content, file, callback) => {
      var nameFile = getPathFile(file.history[0]);
      // Si el archivo esta vacio no se agrega en la lista
      if (content.trim() !== '') {
        listFileHTML[nameFile] = content;
      }
      callback(null, content);
    }))
    .on('end', () => {
      return done;
    });
});

/**
 * @description
 * Busca todos los archivos JSON, lee el contenido de cada archivo y lo agrega
 * al objeto listFileJSON convirtiendo el contenido del archivo en un objeto.
 */
gulp.task('readJson', done => {
  return gulp
    .src(`./${folderJSON}/**/*.json`)
    // Minifica el contenido del archivo JSON
    .pipe(gulpJsonMinify())
    .pipe(gulpEach((content, file, callback) => {
      var nameFile = getPathFile(file.history[0]);
      // Si el archivo esta vacio no se agrega en la lista
      if (content.trim() !== '') {
        listFileJSON[nameFile] = JSON.parse(`${content}`);
      }
      callback(null, content);
    }))
    .on('end', () => {
      return done;
    });
});

/**
 * @description
 * Convierte la ruta absoluta en relativa a la carpeta raÃ­z del proyecto.
 * @param {String} path - Ruta absoluta del archivo.
 * @return {String} Ruta utilizada en el cÃ³digo desde la raÃ­z del proyecto.
 */
function getPathFile(path) {
  return path.replace(__dirname, '').substr(1);
}

//#endregion

//#region Entorno de desarollo

/**
 * @description
 * Ejecuta las tareas para el entorno de desarrollo de la multimedia, realiza el
 * llamado a las funciones y tareas requeridas.
 */
gulp.task('develop', gulpSequence('removeOldFilesDevelop', 'removeOldFilesProduction', 'modifyPreloadEdgeDevelop', 'combineMinifyJsDevelop', 'watchChange'));


/**
 * @description
 * Modifica el archivo de Edge que realiza la precarga para cargar todos los
 * archivos JS en un solo archivo.
 */
gulp.task('modifyPreloadEdgeDevelop', done => {
  return gulp
    .src('./index_edgePreload.js')
    .pipe(gulpReplace(/(aLoader\s\=\s\[\n)((.)+\n)+/g, `aLoader = [\n{ load: './${folderDevelop}/${nameFileDevelop}.js'}];\n\n`))
    .pipe(gulp.dest(`./`));
});

/**
 * @description
 * Combina los archivos para generar el cÃ³digo principal de producciÃ³n, incluye 
 * librerias y archivos de confirugraciÃ³n.
 */
gulp.task('combineMinifyJsDevelop', ['createConfig'], done => {
  return gulp
    .src(filesCombine)
    .pipe(gulpWait(1000 * 1))
    .pipe(gulpConcat(`${nameFileDevelop}.js`))
    .pipe(gulp.dest(`./${folderDevelop}`));
});

/**
 * @description
 * Elimina la carpeta de desarrollo generada anteriomente.
 */
gulp.task('removeOldFilesDevelop', done => {
  return gulp
    .src([`./${folderDevelop}`], {
      read: false
    })
    .pipe(gulpClean());
});


/**
 * @description
 * Al detectar cambios sobre archivos JSON, JS y HTML, realiza la publicaciÃ³n
 * de la multimedia y actualiza el servidor web recargando de forma automatica
 * el navegador.
 */
gulp.task('watchChange', done => {
  return gulp
    .watch([
      `./${folderJS}/**/*.js`,
      `./${folderJSON}/**/*.json`,
      `./${folderHTML}/**/*.html`
    ], ['combineMinifyJsDevelop']);
});

//#endregion

//#region Entorno de producciÃ³n.

/**
 * @description
 * Ejecuta el entorno de producciÃ³n de la multimedia, organiza los elementos
 * necesarios para realizar la entrega del paquete para montaje en la plataforma.
 */
gulp.task('production', gulpSequence('removeOldFilesProduction', 'removeOldFilesDevelop', 'modifyPreloadEdge', 'combineMinifyJsProductionDownload', 'modifyPreloadEdge', 'createZip', 'combineMinifyJsProduction', 'copyPublic', 'removeOldFilesDevelop'));


/**
 * @description
 * Elimina la carpeta anterior de publicaciÃ³n para evitar errores en la
 * generaciÃ³n de la multimedia para producciÃ³n.
 */
gulp.task('removeOldFilesProduction', done => {
  return gulp
    .src([`./${folderPublic}`], {
      read: false
    })
    .pipe(gulpClean());
});


/**
 * @description
 * Modifica el archivo de Edge que realiza la precarga para cargar todos los
 * archivos JS en un solo archivo.
 */
gulp.task('modifyPreloadEdge', done => {
  return gulp
    .src('./index_edgePreload.js')
    .pipe(gulpReplace(/(aLoader\s\=\s\[\n)((.)+\n)+/g, `aLoader = [\n{ load: '${folderJS}/${nameFileMain}.min.js'}];\n\n`))
    .pipe(gulp.dest(`./${folderPublic}`));
});

/**
 * @description
 * Combina todos los archivos pero modifica el archivo config con la opciÃ³n que 
 * indica que es la versiÃ³n descargable de la multimedia.
 */
gulp.task('combineMinifyJsProductionDownload', ['createConfigProduction'], done => {
  return gulp
    .src(filesCombine)
    .pipe(gulpWait(1000 * 1))
    .pipe(gulpConcat(`${nameFileDevelop}.js`))
    .pipe(gulp.dest(`./${folderDevelop}`))
    .pipe(gulpUglify(`./${folderPublic}/${folderJS}/${nameFileMain}.js`, {
      compress: true,
      mangle: true
    }))
    .pipe(gulpRename({
      suffix: '.min'
    }))
    .pipe(gulp.dest(`./`));
});

/**
 * @description
 * Combina todos los archivos para generar la versiÃ³n de producciÃ³n.
 */
gulp.task('combineMinifyJsProduction', ['createConfig'], done => {
  return gulp
    .src(filesCombine)
    .pipe(gulpWait(1000 * 1))
    .pipe(gulpConcat(`${nameFileDevelop}.js`))
    .pipe(gulp.dest(`./${folderDevelop}`))
    .pipe(gulpUglify(`./${folderPublic}/${folderJS}/${nameFileMain}.js`, {
      compress: true,
      mangle: true
    }))
    .pipe(gulpRename({
      suffix: '.min'
    }))
    .pipe(gulp.dest(`./`));
});

/**
 * @description
 * Crea el archivo de configuraciÃ³n en la carpeta donde se almacena el cÃ³dgo JS.
 */
gulp.task('createConfigProduction', ['readHtml'], done => {
  return gulp
    .src(`./${folderJS}/*.js`, {
      read: false
    })
    .pipe(gulpFile(`${nameFileConfig}.js`, createContentFileConfigDownload()))
    .pipe(gulp.dest(`./${folderDevelop}`));
});

/**
 * @description
 * Convierte los objetos que almacena las rutas de los archivos JSON y HTML
 * con su contenido en un String para ser almacenado en una variable. Esta
 * funciÃ³n agrega extra la opciÃ³n de que es la versiÃ³n descargable en la 
 * configuraciÃ³n del archivo.
 * @return {String} Cadena de caracteres con variables utilizadas para
 * indentificar el contenido de los archivos.
 */
function createContentFileConfigDownload() {
  var contentFile = `
  var versionDownload = true;
  var jsonFiles = ${JSON.stringify(listFileJSON)};
  var htmlFiles = ${JSON.stringify(listFileHTML)};
  `;
  return contentFile;
}

/**
 * @description
 * Crea la carpeta ZIP con el contenido de la multimedia y lo almacena en la
 * carpeta de descargas.
 */
gulp.task('createZip', ['copyPublic'], done => {
  return gulp
    .src(`./${folderPublic}/**/*.*`)
    .pipe(gulpZip(nameFileZIP))
    .pipe(gulp.dest(`./${folderPublic}/download`));
});

/**
 * @description
 * Realiza la copia de los archivos que no se pueden combinar y son necesarios
 * para el funcionamiento de la multimedia.
 */
gulp.task('copyPublic', done => {
  return gulp
    .src([
      'images/**/*.*',
      'css/**/*.*',
      'sounds/**/*.*',
      'ar/**/*.*',
      'download/**/*.*',
      'index.html'
    ])
    .pipe(gulpCopy(`./${folderPublic}`));
});

//#endregion